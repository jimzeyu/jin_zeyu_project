import sys
import os
import pandas as pd
import numpy as np
import sqlite3

def csv_to_sqlite(file_name, sep = None):

    if sep == None:

        sep = ','

    else:

        sep = sep

    conn = sqlite3.connect('db.sqlite3')

    df = pd.read_csv(file_name, sep = sep)

    df.to_sql(file_name.replace('.', '_'), conn)


if __name__ == '__main__':

    try:

        csv_to_sqlite(sys.argv[1], sys.argv[2])

    except IndexError:

        csv_to_sqlite(sys.argv[1])
