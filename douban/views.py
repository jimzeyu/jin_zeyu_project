from django.shortcuts import render
from pyecharts import Pie,Gauge,Polar,Funnel
from .ana import DouBanAna_obj, r
import json
from pyecharts import Line
import redis


# Create your views here.

REMOTE_HOST = "https://pyecharts.github.io/assets/js"

def douban1(request):


    try:

        if r.exists('douban1') and r.exists('douban1s'):
            line = Line("电影评论数", "2018-4-23", title_color='blue', width=1400, height=700, background_color='white')
            info_dict = json.loads(r.get('douban1').decode())
            print(info_dict)
            info_dicts = json.loads(r.get('douban1s').decode())

            line.add("电影评论数", list(info_dict.keys()),list(info_dict.values()), is_datazoom_show=True, is_fill=True, line_opacity=0.2,area_opacity=0.4)
            line.add("电影点赞数", list(info_dicts.keys()), list(info_dicts.values()), is_smooth=True, is_fill=True, area_color="#000",area_opacity=0.5)
        else:
            line = DouBanAna_obj.douban1()
    except redis.exceptions.ConnectionError:

        line = DouBanAna_obj.douban1()

    context = dict(

        myechart=line.render_embed(),

        host=REMOTE_HOST,

        script_list=line.get_js_dependencies()
    )


    return render(request, '豆瓣点赞评分数据分析表.html', context)



def douban2(request):
    info = DouBanAna_obj.douban2()
    pingfenss = round(info.Star).ravel()
    dayu = pingfenss[pingfenss > 2.5]
    dabiao = len(dayu) / len(pingfenss) * 100
    gauge = Gauge("平均分大于2.5的电影", width=1200, height=1000)

    gauge.add("平均分大于2.5的电影", "达标率", round(dabiao, 2))
    context = dict(

        myechart=gauge.render_embed(),

        host=REMOTE_HOST,

        script_list=gauge.get_js_dependencies()
    )


    return render(request, '豆瓣平均分里程碑表.html', context)


def douban3(request):
    info = DouBanAna_obj.douban3()
    name = info[0].index.ravel()
    pingfens = round(info[0].Star).ravel()

    pingfenss = round(info[1].Star).ravel()

    polar = Polar("平均分", width=1200, height=1000)
    polar.add("筛过的平均分", pingfens, radius_data=name, type='barRadius', is_stack=True)
    polar.add("没有筛出的平均分", pingfenss, radius_data=name, type='barRadius', is_stack=True)
    context = dict(

        myechart=polar.render_embed(),

        host=REMOTE_HOST,

        script_list=polar.get_js_dependencies()
    )


    return render(request, '豆瓣筛选极坐标表.html', context)


def douban4(request):


    try:

        if r.exists('pl'):
            pie = Pie("评论用户表", width=1200, height=1000)
            info_dict = json.loads(r.get('pl').decode())
            pie.add("", list(info_dict.keys()), list(info_dict.values()), is_label_show=True)

        else:
            pie = DouBanAna_obj.douban4()

    except redis.exceptions.ConnectionError:

        pie = DouBanAna_obj.douban4()


    context = dict(

        myechart=pie.render_embed(),

        host=REMOTE_HOST,

        script_list=pie.get_js_dependencies()
    )

    return render(request,"豆瓣发布评论用户数量表.html",context)

def douban5(request):


    try:

        if r.exists('fuchou'):

            funnel = Funnel('分析漏斗图', width=1200, height=1000)
            info_dict = json.loads(r.get('fuchou').decode())

            funnel.add('', list(info_dict.keys()), list(info_dict.values()), is_label_show=True, label_pos='inside', label_text_color='#fff')

        else:
            funnel = DouBanAna_obj.douban5()

    except redis.exceptions.ConnectionError:

        funnel = DouBanAna_obj.douban5()



    context = dict(

        myechart=funnel.render_embed(),

        host=REMOTE_HOST,

        script_list=funnel.get_js_dependencies()
    )

    return render(request,"豆瓣复仇者联盟2日期用户表.html",context)


