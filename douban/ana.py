import pandas as pd
import numpy as np
import sqlite3
import json
import redis
from pyecharts import Line,Pie,Funnel
import socket

# Create your views here.

if __name__ == '__main__':

    db_path = '../db.sqlite3'

else:

    db_path = 'db.sqlite3'


r = redis.Redis(host='192.168.43.253', port=6379, password = '1', db = '0')

class DouBanAna:

    def __init__(self):

        self.conn = sqlite3.connect(db_path)

        sql = 'SELECT * FROM DMSC_csv'

        self.df = pd.read_sql(sql, self.conn)


    def douban1(self):

        info = self.df.groupby(self.df["Movie_Name_CN"]).sum()["Like"]
        infos = self.df.groupby(self.df["Movie_Name_CN"]).sum()["Star"]
        info_dict = info.to_dict()
        info_dicts = infos.to_dict()

        try:

            r.set('douban1', json.dumps(info_dict))
            print(info_dict)
            r.set('douban1s', json.dumps(info_dicts))

        except redis.exceptions.ConnectionError:
            pass

        line = Line("电影评论数", "2018-4-22", width=1400, height=700, background_color='white')
        line.add("电影评论数", info.index, info.values, is_datazoom_show=True, is_fill=True, line_opacity=0.2,
                 area_opacity=0.4)
        line.add("电影点赞数", infos.index, infos.values, is_smooth=True, is_fill=True, area_color="#000",
                     area_opacity=0.5)
        return line

    def douban2(self):
        pingfen = self.df.groupby([self.df["Movie_Name_CN"]]).mean()

        return pingfen

    def douban3(self):
        a = self.df[self.df["Like"] > 100]
        pingfen = a.groupby([a["Movie_Name_CN"]]).mean()
        pf = self.df.groupby([self.df["Movie_Name_CN"]]).mean()

        pingfen_list = [pingfen,pf]

        return pingfen_list

    def douban4(self):
        pl = self.df["Username"].value_counts().head(20)
        pie = Pie("评论用户表", width=1200, height=1000)
        info_dict = pl.to_dict()
        try:

            r.set('pl', json.dumps(info_dict))

        except redis.exceptions.ConnectionError:
            pass

        pie = Pie("评论用户表", width=1200, height=1000)
        pie.add("", pl.index, pl.values, is_label_show=True)


        return pie

    def douban5(self):
        df = self.df[self.df["Movie_Name_CN"].str.contains("复仇者联盟2")]
        funnel = Funnel('分析漏斗图', width=1200, height=1000)
        df1 = df["Date"].value_counts().head(5)
        info_dict = df1.to_dict()
        try:

            r.set('fuchou', json.dumps(info_dict))



        except redis.exceptions.ConnectionError:
            pass

        funnel.add('', df1.index, df1.values, is_label_show=True, label_pos='inside', label_text_color='#fff')

        return funnel


DouBanAna_obj = DouBanAna()

if __name__ == '__main__':

    x = DouBanAna_obj